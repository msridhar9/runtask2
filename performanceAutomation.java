package com.company;

// Import statements
import java.io.*;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.*;
import java.time.*;
import com.microsoft.azure.storage.*;

// Create a class called performanceAutomation (no parameters)
public class performanceAutomation {
    // Class variable/constant definitions
    private static final String tenantId = "2f611596-a3da-4a81-94e8-fd4483868fc1";
    private static final String subscriptionId = "0d87997e-5697-4ce8-9b84-a045b0969f69";
    private static final String resourceGroupName = "rg-ib-ue1-prod-mdt-01";
    private static final String factoryName = "df-ib-ue1-prod-mdt-01";
    private static final String api_vers = "2018-06-01";
    private static final String client_id = "15441b8e-0599-4c71-87d8-69174e037637";
    private static final String content = "grant_type=client_credentials";
    private static final String client_secret = "Hnf.yy3TO2b~2~HW5OSAPbd41.SzGR9yHY";
    private static final String resource = "https://management.azure.com/";
    private static final String storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=dlsibue1qamdt01;AccountKey=OrihbIkyvrAoFwyoX26/3RCq+YxZBoWFuxNEYjhnUf8rvIbyfbVbl09cuCChpPukJXljGMidpa1Xhx/EdAeNPA==;EndpointSuffix=core.windows.net";
    private static ArrayList<String> runId_vec = new ArrayList<String>();
    private static ArrayList<String> pipeline_name_vec = new ArrayList<String>();
    private static ArrayList<Integer> total_exec_dur_vec = new ArrayList<>();
    private static ArrayList<Integer> job_durations_vec = new ArrayList<Integer>();
    private static Workbook workbook;
    private static Workbook workbook2;
    private static String today_date;
    private static String activity_name;
    private static String activity_run_start;
    private static String activity_run_end;
    private static int totalExecDur;
    private static int job_duration;
    private static JSONObject output_obj;

    // Create a counter: used to increase the counter of total duration vec so that each pipeline.xlsx file contains only one row with total duration
    private static int counter = 0;

    // Makes the rest api call to login.microsoftonline.com to obtain bearer token
    public static String restAPICallToLogin() throws UnirestException {
        // Assemble login.microsoftonline.com rest api url using Azure AD tenant ID to retrieve bearer token
        String login_url = "https://login.microsoftonline.com/" + tenantId + "/oauth2/token";
        // Do a post request to the above url using Java Unirest Package and retrieve response in JSON format
        HttpResponse<JsonNode> response = Unirest.post(login_url)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .header("Cookie", "x-ms-gateway-slice=estsfd; stsservicecookie=estsfd; fpc=Aie1sfmBodlDnvFCJJRYBapYiy5uAQAAANSWR9cOAAAA")
                .field("grant_type", "client_credentials")
                .field("client_id", client_id)
                .field("client_secret", client_secret)
                .field("resource", resource)
                .asJson();

        // Obtain response body and store it in a JSON object for parsing
        org.json.JSONObject obj = response.getBody().getObject();
        // Parse out the bearer token string from the JSON object and store it in a variable called token for later use
        String token = obj.getString("access_token");
        return token;
    }

    // Makes the rest api call to azure management api to obtain pipeline run IDs
    public static void restAPICallToAzure(String token) throws UnirestException, IOException {
        // Assemble management.azure.com rest api url using subscriptionId, resourceGroupName, factoryName, and api version
        String azure_management_url_1 = resource + "/subscriptions/" + subscriptionId + "/resourceGroups/" + resourceGroupName + "/providers/Microsoft.DataFactory/factories/" + factoryName + "/queryPipelineRuns?api-version=" + api_vers;
        // Do a post request to the above url using Java Unirest Package and retrieve response in JSON format
        HttpResponse<JsonNode> response2 = Unirest.post(azure_management_url_1)
                .header("Authorization", "Bearer " + token)
                .asJson();
        // Obtain response body and store it in a JSON object for parsing
        org.json.JSONObject obj1 = response2.getBody().getObject();
        // Parse out the values found in the JSON object and store them in a JSON array called values
        JSONArray values = obj1.getJSONArray("value");
        for (int i = 0; i < values.length(); i++)
        {
            JSONObject values_obj = (JSONObject) values.get(i);
            String pipelineName = (String) values_obj.get("pipelineName");
            try {
                totalExecDur = (Integer) values_obj.get("durationInMs");
            }
            catch (ClassCastException e){
                System.out.println("Total Execution Duration not found");
            }
            String runId = (String) values_obj.get("runId");
            runId_vec.add(runId);
            // Add the pipeline names to the pipeline_name_vec for later use
            pipeline_name_vec.add(pipelineName);
            // Add the total execution durations to the total_exec_dur_vec for later use
            total_exec_dur_vec.add(totalExecDur);
            // Create/update the excel files with the pipeline name to upload data
            excelFileCreation(pipelineName);
        }
    }

    // Creates/updates excel files with the pipeline names and adds fields to the files to store data
    public static void excelFileCreation(String pipelineName) throws IOException {
        // Obtain today's date and store it in a string
        LocalDate date = LocalDate.now();
        today_date = date.toString();
        // Check if there is an excel file with the name of this pipeline in the user's current directory. If not, a file with this name must be created now.
        String fileName = System.getProperty("user.dir") + "/" + pipelineName + ".xlsx";
        // If file doesn't exist, create a new excel workbook, sheet, and row, and add in headers for the activity name, start and end time, individual job duration, and total execution duration
        if (!new File(fileName).exists()) {
            workbook = new XSSFWorkbook();
        }
        // If file already exists, create a new excel workbook, set up a file input stream and load the excel file in to it
        else {
            // System.out.println("File exists.");
            FileInputStream input_excel = new FileInputStream(fileName);
            workbook = new XSSFWorkbook(input_excel);
        }
        // Create a new sheet and row, and add in headers for the activity  name, start and end time, individual job duration, and total execution duration
        workbook.createSheet(today_date);
        Sheet current_sheet = workbook.getSheet(today_date);
        Row current_row = current_sheet.createRow(0);
        current_row.createCell(0).setCellValue("Activity Name");
        current_row.createCell(1).setCellValue("Activity Start Time");
        current_row.createCell(2).setCellValue("Activity End Time");
        current_row.createCell(3).setCellValue("Job Duration");
        current_row.createCell(4).setCellValue("Data Read (Size, Bytes)");
        current_row.createCell(5).setCellValue("Data Written (Size, Bytes)");
        current_row.createCell(6).setCellValue("Total Execution Duration");
        // Write this workbook out to a file output stream and close the file to update the excel file in the directory specified
        FileOutputStream newFile = new FileOutputStream(fileName);
        workbook.write(newFile);
        newFile.close();
    }

    // Makes the rest api call to azure management api to obtain pipeline activity run information using pipeline run IDs found earlier
    public static void restAPICallToAzure2(String token) throws UnirestException, IOException {
        for (int j = 0; j < runId_vec.size(); j++) {
            String azure_management_url_2 = resource + "/subscriptions/" + subscriptionId + "/resourceGroups/" + resourceGroupName + "/providers/Microsoft.DataFactory/factories/" + factoryName + "/pipelineruns/" + runId_vec.get(j) + "/queryActivityruns?api-version=" + api_vers;
            // Do a post request to the above url using Java Unirest Package and retrieve response in JSON format
            HttpResponse<JsonNode> response2 = Unirest.post(azure_management_url_2)
                    .header("Authorization", "Bearer " + token)
                    .asJson();
            // Obtain response body and store it in a JSON object for parsing
            org.json.JSONObject obj2 = response2.getBody().getObject();
            // Parse out the values found in the JSON object and store them in a JSON array called values
            JSONArray values2 = obj2.getJSONArray("value");
            for (int k = 0; k < values2.length(); k++) {
                // Convert each pipeline's information (parsing through the values JSON array) into a JSON object
                JSONObject values2_obj = (JSONObject) values2.get(k);
                // System.out.println(values2_obj);
                // Get activity name, start and end time, and individual job duration and store them inside variables for later use
                activity_name = (String) values2_obj.get("activityName");
                activity_run_start = (String) values2_obj.get("activityRunStart");
                activity_run_end = (String) values2_obj.get("activityRunEnd");
                job_duration = (Integer) values2_obj.get("durationInMs");
                output_obj = (JSONObject) values2_obj.get("output");
                excelFileUpdates(pipeline_name_vec.get(j));
            }
            // Increment the counter during every iteration of the for loop
            counter = counter + 1;
        }
    }

    // Updates the excel files for each pipeline with information about activities they perform and durations as well as data size transferred
    public static void excelFileUpdates(String pipelineName) throws IOException {
        // Check if there is an excel file with the name of this pipeline in the existing directory (extra check)
        String excelfileName = System.getProperty("user.dir") + "/" + pipelineName + ".xlsx";
        // If file exists (it should), create a new excel workbook, set up a file input stream and load the excel file in to it
        if (new File(excelfileName).exists()) {
            FileInputStream inputStream = new FileInputStream(new File(excelfileName));
            workbook2 = new XSSFWorkbook(inputStream);
        }
        // Obtain the sheet corresponding to today's date and add a row (based on the last row in the sheet, should increment every iteration to add more rows)
        Sheet current_sheet = workbook2.getSheet(today_date);
        int current_row = current_sheet.getLastRowNum();
        int now_row = current_row + 1;
        Row new_row = current_sheet.createRow(current_row + 1);
        // Fill in activity name, activity start and end times, and individual job durations for each activity found for the pipeline
        new_row.createCell(0).setCellValue(activity_name);
        new_row.createCell(1).setCellValue(activity_run_start);
        new_row.createCell(2).setCellValue(activity_run_end);
        new_row.createCell(3).setCellValue(job_duration);
        new_row.createCell(4);
        new_row.createCell(5);
        if (!output_obj.keySet().contains("dataRead"))
        {
            new_row.getCell(4).setCellValue("Not available");
        }
        else
        {
            String data_read_size = JSONObject.valueToString(output_obj.get("dataRead"));
            new_row.getCell(4).setCellValue(data_read_size);
        }
        if (!output_obj.keySet().contains("dataWritten"))
        {
            new_row.getCell(5).setCellValue("Not available");
        }
        else
        {
            String data_written_size = JSONObject.valueToString(output_obj.get("dataWritten"));
            new_row.getCell(5).setCellValue(data_written_size);
        }

        // Fill in total execution duration if the current row being filled is the first row, since there is only one cell of data for this parameter
        if (now_row == 1) {
            new_row.createCell(6).setCellValue(total_exec_dur_vec.get(counter));
        }
        // Write this workbook out to a file output stream and close the file to update the excel file in the directory specified
        FileOutputStream newFile2 = new FileOutputStream(excelfileName);
        workbook2.write(newFile2);
        newFile2.close();
     }

     // Uploads excel files to blob storage container and specified file path
    public static void uploadToBlobStorage() throws URISyntaxException, InvalidKeyException, StorageException, IOException {
        // Create a cloud storage account object named storage account with the connection string
        CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);
        // Create a cloud blob client object with the storage account
        CloudBlobClient cloudBlobClient = storageAccount.createCloudBlobClient();
        // Get the specific container's reference by supplying its name to the cloud blob client and store it in a cloud blob container object named container
        CloudBlobContainer container = cloudBlobClient.getContainerReference("fsqamdt01");
        // The cloud blob client has been created and now connects to the storage account and the specific container
        // For each excel file with pipeline run data, perform the upload process by getting the block blob reference and using the upload method in a for loop
        for (int m = 0; m < runId_vec.size(); m++)
        {
            // store file path to excel file in a variable
            final String filePath = System.getProperty("user.dir") + "/" + pipeline_name_vec.get(m) + ".xlsx";
            // split file path to excel file name and store in a variable
            final String halfFilePath = pipeline_name_vec.get(m) + ".xlsx";
            // Get block blob reference to storage account container named pipelines performance test runs/pipelineName/pipelineName.xlsx so that blob can upload to this file name
            CloudBlockBlob blob = container.getBlockBlobReference("pipelines performance test runs/" + pipeline_name_vec.get(m) + "/" + halfFilePath);
            // Upload actual excel file from directory to blob referenced above
            blob.uploadFromFile(filePath);
        }
    }
    public static void main(String[] args) throws UnirestException, IOException, InvalidKeyException, StorageException, URISyntaxException {
        // Obtain the bearer token by calling the restAPICallToLogin method
        String bearer_token = restAPICallToLogin();
        // Using the bearer token obtained above, submit another post request to Azure Management API to retrieve all pipelines' runIDs and return JSON Response in Array format
        restAPICallToAzure(bearer_token);
        // Using the bearer token obtain above, submit one last post request to Azure Management API to retrieve all pipelines' activity run information using the above parsed out runIDs
        restAPICallToAzure2(bearer_token);
        // Upload updated excel files to blob storage and finish the process
        uploadToBlobStorage();
        // FINAL CHECKPOINT: Print out that the entire process is complete and finish the task.
        System.out.println("The entire process is complete. Thank you.");
    }
}
